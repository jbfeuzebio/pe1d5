import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email;
  senha;
  loginForm: FormGroup;

  constructor(
      
      private builder: FormBuilder,
      private service: LoginService
  ) { }

  ngOnInit() {
    this.loginForm = this.builder.group({
      email: ['',[Validators.required, Validators.email]],
      senha: ['',[Validators.required, Validators.minLength(8)]]
    });
  }

  login(){
    const user = this.loginForm.value;
    this.service.login(user);
  
  }



}
